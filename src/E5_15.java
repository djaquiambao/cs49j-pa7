////////////////////////////////////////
//  Author: Darwish Quiambao
//  Course Number: CS49J
//  Date Created: 11/2/2020
//  Description: This program demonstrates passing of argument namely Filename into program. More importantly,
//  I used LinkedHashMap and ArrayList to read the lines and store the identifier/index pairs into the map and arraylist.
//  This program finds occurrences of keywords (identifiers) in a File provided in the argument.
//  ***********************
//
///////////////////////////////////////
import java.io.*;
import java.util.*;
import java.util.regex.Pattern;

public class E5_15 {

    public static void main(String[] args) {

        //pass in file argument into main
        if(args.length > 0)
        {
            //pass in the argument to instantiated File object
            File fIn = new File(args[0]);
            //Declare a Scanner for reading from the source file
            try (Scanner in = new Scanner(fIn); FileReader fileIn = new FileReader(args[0]); fileIn) {

                //Used to read the entire Main.java file and later store it into a String variable named 'line'
                LineNumberReader lineNumberReader = new LineNumberReader(fileIn);


                String line = "";

                //Read anything other than the Characters/string
                //that includes the following RegExp
                in.useDelimiter("[^A-Za-z0-9_]+");

                //I use LinkedHashMap to maintain insertion order.
                Map<String, Integer> queueMap = new LinkedHashMap<>();
                //Stores the entire line of string we will be comparing identifiers to.
                ArrayList<String> q = new ArrayList<>();
                int counter = 0;

                //Read through file
                while (in.hasNext()) {
                    //Contains both the index and the identifier. Since this is a map, the last occurrence of the SAME key is stored.
                    //Counter variable references the index which is retained in the map.
                    queueMap.put(in.next(), counter++);
                }
                //close
                in.close();

                //read the file again and store into string variable and add it to arraylist
                while ((line = lineNumberReader.readLine()) != null) {
                    //We just add the entire string of text from Main.java into an ArrayList to compare with the map and find occurrence of a word.
                    q.add(line);
                }
                lineNumberReader.close();

                //reset variable counter to use with the display.
                counter = 0;

                //Use for matching exact identifier with string line
                String temp;

                for (Map.Entry<String, Integer> ent : queueMap.entrySet()) {
                    System.out.println(counter++ + ": " + ent.getKey() + " occurs in: ");
                    for (String s : q) {
                        //using regex, we find the exact match of keyword/identifier in the lines
                        temp = ".*\\b" + Pattern.quote(ent.getKey()) +"\\b.*";
                        //if there is a match for the specified pattern, we print out which lines
                        if (s.matches(temp)) {
                            System.out.println(s);
                        }

                    }
                }
            } catch (FileNotFoundException f) {
                System.out.println("File not found.");
                f.getStackTrace();
            } catch (NullPointerException n) {
                n.getStackTrace();
                System.out.println(n.getMessage());
            } catch (IOException io)
            {
              io.getStackTrace();
              System.out.println("IOException caught!");
            }

        }
        else
            System.out.println("Need a command line argument for the file to read");
    }
}
