////////////////////////////////////////
//  Author: Darwish Quiambao
//  Course Number: CS49J
//  Date Created: 11/1/2020
//  Description: Reimplements the E15_4 exercise to have a Student class which contains the student name (first and last)
//  and unique integer ID. In this program, I also included a private letter grade variable for grade keeping.
//  The class implements the Comparable interface and overrides the methods compareTo, toString, equals and hashcode
//  Because we are overriding hashcode, I also included the treeMap that sorts students based on first name first,
//  if not the last name, and the very last is Integer ID. These are all parsed into a string and passed into the TreeMap
//  for sorting.
//  ***********************
//
///////////////////////////////////////
package P15_2;

import java.util.*;

class Student implements Comparable<Student>
{
    private final String firstName;
    private final String lastName;
    private String letterGrade;
    private final int nID;

    //For sorting the names based on the LastName first-, if not the firstName, if not the StudentID
    //Maintained as part of the Class itself not an instance of a class.
    public static Map<String, String> studentNames = new TreeMap<>();

    public Student(String firstName, String lastName, String letterGrade, int nID)
    {
        this.firstName = firstName;
        this.lastName = lastName;
        this.nID = nID;
        this.letterGrade = letterGrade;

        //parse strings into one and then store it into the treeMap for Sorting as KEYS the letter grade is the value.
        String fullName_ID = this.getLastName() + ", " + this.getFirstName() + " (ID=" + this.getnID() + "): ";
        studentNames.put(fullName_ID, letterGrade);
    }

    @Override
    public String toString() {
        return getFirstName() + " " + getLastName() + " (ID="
                + getnID() + "): " + getLetterGrade();
    }

    //Manual overriding of compareTo implemented here.
    //I evaluate if the student last name of current object is the same as the passed student object, otherwise, last name
    // else uniqueID
    @Override
    public int compareTo(Student o) {

        if(this.getLastName().equals(o.getLastName()))
        {
            if(this.getFirstName().equals(o.getFirstName()))
            {
                return Integer.compare(this.getnID(), o.getnID());
            }
            else
                return this.getFirstName().compareTo(o.getFirstName());
        }

        else
            return this.getLastName().compareTo(o.getLastName());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Student student = (Student) o;
        return getLastName().equals(student.getLastName()) &&
                Objects.equals(getFirstName(), student.getFirstName()) &&
                getnID() == student.getnID();
    }

    //Using .hash() to override hashCode().
    @Override
    public int hashCode() {
        return Objects.hash(getLastName(), getFirstName(), getnID());
    }

    //Getter methods for the private members of the Student Class.
    public int getnID()
    {
        return nID;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getLetterGrade() {
        return letterGrade;
    }

    //Only setting letter grade is expected so this is the only setter method
    public void setLetterGrade(String student, String letterGrade) {
        this.letterGrade = letterGrade;
        //Update the Grade inside the Student TreeMap that sorts students
        studentNames.replace(student, letterGrade);
    }
}

public class P15_2 {
    public static void main(String[] args) {

        //HashMap to store students in
        Map<Integer, Student> studentMap = new HashMap<>();

        //Reads user input
        Scanner userInput = new Scanner(System.in);
        System.out.println("Select an option: \n\t(a) to add a student\n\t(r) to remove a student\n\t(m) to modify a grade"
                + "\n\t(p) to print all grades\n\t(q) to quit");

        try {
            //Read in user choice:
            String strIn = userInput.nextLine();
            //Student name and grades
            String lastName = "", firstName = "", grade = "";
            //Unique Integer ID to be passed into student class constructor.
            int nID = 0;

            //Run loop as long as q is not selected as an option.
            while (!strIn.equalsIgnoreCase("q")) {
                //Evaluate option from user input
                switch (strIn.toLowerCase()) {
                    case "a" -> {
                        System.out.print("Enter student last name: ");
                        lastName = userInput.nextLine();
                        System.out.print("Enter student first name: ");
                        firstName = userInput.nextLine();

                        System.out.print("Enter student ID: ");
                        nID = userInput.nextInt();
                        userInput.nextLine();

                        //Check if ID is already taken
                            while(studentMap.containsKey(nID))
                            {
                                System.out.println("ID already taken!\nPlease re-enter a new student ID.");
                                System.out.print("Enter student ID: ");
                                nID = userInput.nextInt();
                                userInput.nextLine();
                            }

                        System.out.print("Enter student grade: ");
                        grade = userInput.nextLine();

                        studentMap.put(nID, new Student(firstName, lastName, grade, nID));
                    }
                    case "r" -> {
                        try {
                            System.out.print("Enter student ID to be removed: ");
                            nID = userInput.nextInt();
                            userInput.nextLine();
                            //To locate the student inside the TreeMap.
                            String strTempStudent = studentMap.get(nID).getLastName() + ", " + studentMap.get(nID).getFirstName() + " (ID=" + studentMap.get(nID).getnID() + "): ";
                            //Remove student inside the treemap.
                            Student.studentNames.remove(strTempStudent);
                            System.out.println("Student " + studentMap.get(nID).getFirstName() + ", " + studentMap.get(nID).getLastName() + " removed.");
                            //Then remove the Student object
                            studentMap.remove(nID);
                        }catch(NullPointerException n)
                        {
                            n.getStackTrace();
                            System.out.println(n.getMessage());
                        }
                    }
                    case "m" -> {
                        try {
                            System.out.print("Enter student ID: ");
                            nID = userInput.nextInt();
                            userInput.nextLine();
                            System.out.print("What would you like to change the grade to? (Current Grade: "
                                    + studentMap.get(nID).getLetterGrade() + "): ");
                            grade = userInput.nextLine();
                            //Store temporarily into a string for updating student TreeMap.
                            String strTempStudent = studentMap.get(nID).getLastName() + ", " + studentMap.get(nID).getFirstName() + " (ID=" + studentMap.get(nID).getnID() + "): ";
                            //Update the letter grade by passing in the temporary string
                            studentMap.get(nID).setLetterGrade(strTempStudent, grade);
                            System.out.println("Student " + studentMap.get(nID).getFirstName() + " "
                                    + studentMap.get(nID).getLastName() + "'s grade has been changed to: "
                                    + studentMap.get(nID).getLetterGrade());
                        }catch(NullPointerException n)
                        {
                            n.getStackTrace();
                            System.out.println(n.getMessage());
                        }
                    }
                    case "p" -> {
                        for (Map.Entry<String, String> ent : Student.studentNames.entrySet())
                        {
                            System.out.println(ent.getKey() + ent.getValue());
                        }

                    }
                    default -> {
                        System.out.println("Invalid input...Please try again.");
                    }
                }
                System.out.println("Select an option: \n\t(a) to add a student\n\t(r) to remove a student\n\t(m) to modify a grade"
                        + "\n\t(p) to print all grades\n\t(q) to quit");
                strIn = userInput.nextLine();
            }
        } catch(NullPointerException ne)
        {
            System.out.println(ne.getMessage());
            ne.getStackTrace();
        }
        catch(InputMismatchException in)
        {
            in.getStackTrace();
            System.out.println("An input mismatch exception has occurred...please restart the program to try again");
        }
        catch(Exception e)
        {
            System.out.println("Exception was caught. Exiting program...");
        }
        System.out.println("Thank you for using the program!");

    }
}
