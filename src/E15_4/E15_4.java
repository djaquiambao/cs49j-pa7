////////////////////////////////////////
//  Author: Darwish Quiambao
//  Course Number: CS49J
//  Date Created: 10/29/2020
//  Description: Implements a student - grade pairing using TreeMap
//  The treemap sorts the KEYS in a natural order and takes two strings as types for Key Value pairing.
//
//  ***********************
//
///////////////////////////////////////
package E15_4;

import java.util.Map;
import java.util.Scanner;
import java.util.TreeMap;

public class E15_4 {
    public static void main(String[] args) {

        Map<String, String> student = new TreeMap<>();
        Scanner userInput = new Scanner(System.in);
        System.out.println("Select an option: \n\t(a) to add a student\n\t(r) to remove a student\n\t(m) to modify a grade"
                + "\n\t(p) to print all grades\n\t(q) to quit");
        try {
            String strIn = userInput.nextLine();
            //Student name and grades
            String studentName = "", grades = "";

            //Run loop as long as q is not selected as an option.
            while (!strIn.equalsIgnoreCase("q")) {
                //Evaluate option from user input
                switch (strIn.toLowerCase()) {
                    case "a" -> {
                        System.out.print("Enter student name: ");
                        studentName = userInput.nextLine();
                        System.out.print("Enter student grade: ");
                        grades = userInput.nextLine();

                        student.put(studentName, grades);
                    }
                    case "r" -> {
                        System.out.print("Enter student name to be removed: ");
                        studentName = userInput.nextLine();
                        student.remove(studentName);
                        System.out.println("Student " + studentName + " removed.");
                    }
                    case "m" -> {
                        System.out.print("Enter student name: ");
                        studentName = userInput.nextLine();
                        System.out.print("What would you like to change the grade to? (Current Grade: "
                                + student.get(studentName) + "): ");
                        grades = userInput.nextLine();
                        student.replace(studentName, grades);
                        System.out.println("Student " + studentName + "'s grade has been changed to: " + student.get(studentName));
                    }
                    case "p" -> {
                        for (Map.Entry<String, String> ent : student.entrySet())
                        {
                            System.out.println(ent.getKey() + ": " + ent.getValue());
                        }
                    }
                    default -> {
                        System.out.println("Invalid input...Please try again.");
                    }
                }
                System.out.println("Select an option: \n\t(a) to add a student\n\t(r) to remove a student\n\t(m) to modify a grade"
                        + "\n\t(p) to print all grades\n\t(q) to quit");
                strIn = userInput.nextLine();
            }
        } catch(NullPointerException ne)
        {
            ne.getMessage();
        } catch(Exception e)
        {
            e.printStackTrace();
        }
        System.out.println("Thank you for using the program!");
    }


}
